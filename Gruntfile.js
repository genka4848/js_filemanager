module.exports = function (grunt) {

    var jsConcatOptions = {
        separator: '',
        banner: "'use strict';",
        process: function (src, filepath) {
            return '\n\n/* Source: ' + filepath + '*/\n' +
                src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
        }
    };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            js_app: {
                options: jsConcatOptions,
                src: [
                    'application/app.js',
                    'application/modules/**/init.js',
                    'application/modules/**/*.js',
                    'application/template.js'
                ],
                dest: 'application/app.min.js'
            },
            js_assets: {
                options: jsConcatOptions,
                src: ['assets/js/jquery/*.js', 'assets/js/**/*.js'],
                dest: 'assets/assets.min.js'
            },
            css: {
                options: {
                    separator: '',
                    process: function (src, filepath) {
                        return '\n\n/* Source: ' + filepath + '*/\n' + src;
                    }
                },
                src: ['assets/css/**/*.css'],
                dest: 'assets/style.css'
            }
        },
        ngtemplates:  {
            app:        {
                src:      'application/templates/**/*.tpl.html',
                dest:     'application/template.js',
                options:    {
                    htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true }
                }
            }
        },
        watch: {
            all: {
                files: [
                    'application/app.js',
                    'application/modules/**/*.js'],
                tasks: ['concat'],
                options: {
                    livereload: true
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-angular-templates');

    // Default task(s).
    grunt.registerTask('default', ['ngtemplates', 'concat:js_app', 'concat:js_assets', 'concat:css']);
    grunt.registerTask('live', ['watch']);

};