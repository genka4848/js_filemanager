angular.module('app').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('application/templates/bookmarks.tpl.html',
    "<div><file-list data=\"bookmarks\" data-bookmark></file-list></div>"
  );


  $templateCache.put('application/templates/create_file.tpl.html',
    "<div ng-controller=\"CreateFilePopupCtrl\"><form ng-submit=\"saveFile($event)\"><div class=\"form-group\"><label for=\"fileName\">Name</label><input id=\"fileName\" type=\"text\" class=\"form-control\" ng-model=\"fileName\" required></div><div class=\"form-group\"><label for=\"fileTypeSelect\">Type</label><select id=\"fileTypeSelect\" class=\"form-control\" ng-model=\"fileType\" ng-options=\"item.type for item in types track by item.mime\"></select></div><div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button> <button type=\"submit\" class=\"btn btn-success\">Save</button></div></form></div>"
  );


  $templateCache.put('application/templates/edit_file.tpl.html',
    "<div class=\"row\"><div class=\"col-md-12\"><div class=\"file-edit-panel\"><h2>Edit File</h2><div class=\"file-edit-container\"><minieditor ng-model=\"content\" options=\"options\"></minieditor></div><div class=\"file-edit-buttons\"><a href=\"#!/\" class=\"btn btn-default btn-sm pull-right\">Cancel</a> <button type=\"button\" class=\"btn btn-default btn-sm pull-right\" ng-click=\"save()\">Save</button></div></div></div></div>"
  );


  $templateCache.put('application/templates/fb_user_images.tpl.html',
    "<div ng-controller=\"FBListImagesCtrl\"><div class=\"list-group\"><a class=\"list-group-item\" ng-repeat=\"img in images\" ng-click=\"select(img)\"><img ng-src=\"{{ img.picture }}\" width=\"70px\" height=\"70px\"></a></div></div>"
  );


  $templateCache.put('application/templates/file_list.tpl.html',
    "<div><file-list data=\"files\"></file-list></div>"
  );


  $templateCache.put('application/templates/file_list_dir.tpl.html',
    "<div class=\"row\"><div class=\"col-md-9 col-sm-8\"><div class=\"list-files-block\"><table class=\"table table-hover list-files\"><thead><tr><th><div ng-click=\"order('name')\"><span>File Name</span> <span class=\"sortorder\" ng-show=\"predicate === 'name'\" ng-class=\"{ reverse: reverse }\"></span></div></th><th><div ng-click=\"order('type')\"><span>File Type</span> <span class=\"sortorder\" ng-show=\"predicate === 'type'\" ng-class=\"{ reverse: reverse }\"></span></div></th><th><div ng-click=\"order('formatSize')\"><span>Size(Kb)</span> <span class=\"sortorder\" ng-show=\"predicate === 'formatSize'\" ng-class=\"{ reverse: reverse }\"></span></div></th></tr></thead><tbody><tr ng-repeat=\"file in files | filter: filterByName | orderBy: predicate: reverse\" ng-click=\"select($event, file)\"><td>{{ file.name }}</td><td>{{ file.type }}</td><td>{{ file.formatSize }}</td></tr></tbody></table><div class=\"input-group col-xs-4\"><span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span> <input type=\"text\" class=\"form-control\" ng-model=\"regex\" placeholder=\"filter by file name\" aria-describedby=\"basic-addon1\"></div></div></div><div class=\"col-md-3 col-sm-4\"><div class=\"panel panel-success\"><div class=\"panel-heading\"><h3 class=\"panel-title\">Actions</h3></div><div class=\"panel-body\"><div ng-show=\"!fileSelected\"><span>Select file at the left side by clicking on it</span></div><div class=\"file-action\" ng-show=\"fileSelected\"><div ng-show=\"fileSelected.action.download\"><a class=\"btn btn-link btn-sm btn-action download-link\">Download</a></div><div ng-show=\"fileSelected.action.bookmark\"><button type=\"button\" class=\"btn btn-link btn-sm btn-action\" ng-click=\"addBookmark()\">Bookmark</button></div><div ng-show=\"fileSelected.action.delete\"><button type=\"button\" class=\"btn btn-link btn-sm\" ng-click=\"removeBookmark()\">Remove</button></div><div ng-show=\"fileSelected.action.preview\"><button type=\"button\" class=\"btn btn-link btn-sm btn-action\" ng-click=\"preview()\">Preview</button></div><div ng-show=\"fileSelected.action.edit\"><button type=\"button\" class=\"btn btn-link btn-sm btn-action\" ng-click=\"edit()\">Edit</button></div></div></div><div class=\"panel-footer\"><span ng-show=\"fileSelected\">File Selected:</span> <span><strong>{{ fileSelected.name }}</strong></span></div></div></div></div>"
  );


  $templateCache.put('application/templates/load_fb_file.tpl.html',
    "<div><div><a class=\"btn btn-link\" ng-click=\"showUserImages($event)\">Upload From Facebook</a></div></div>"
  );


  $templateCache.put('application/templates/messenger.tpl.html',
    "<div class=\"alert alert-dismissible custom-alert\" ng-class=\"alertType\" role=\"alert\" style=\"display: none\"><button type=\"button\" class=\"close\" aria-label=\"Close\" ng-click=\"close()\"><span aria-hidden=\"true\">&times;</span></button> <span class=\"message\"></span></div>"
  );


  $templateCache.put('application/templates/modal_wrapper.tpl.html',
    "<div class=\"modal fade\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button type=\"button\" class=\"close btn-action\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button><h4 class=\"modal-title\">{{ title }}</h4></div><div class=\"modal-body\" ng-include=\"template\"></div></div></div></div>"
  );


  $templateCache.put('application/templates/preview_img.tpl.html',
    "<div><img class=\"preview-loaded-file\" ng-src=\"{{ src }}\"></div>"
  );


  $templateCache.put('application/templates/preview_web_page.tpl.html',
    "<div><iframe class=\"preview-loaded-file\" width=\"560\" height=\"315\" ng-src=\"{{ src }}\" frameborder=\"0\"></iframe></div>"
  );


  $templateCache.put('application/templates/select_img_from_library.tpl.html',
    "<div ng-controller=\"SelectImgModalCtrl\"><div class=\"list-group\"><a class=\"list-group-item\" ng-repeat=\"img in images\" ng-click=\"setImage(img)\">{{ img.name}}</a></div></div>"
  );


  $templateCache.put('application/templates/upload.tpl.html',
    "<div class=\"row\"><div class=\"col-sm-7 col-md-6\"><div class=\"file-select-block\"><div class=\"row\"><div class=\"col-md-6 col-sm-6\"><facebook-file-load></facebook-file-load></div><div class=\"col-md-6 col-sm-6\"><local-file-upload></local-file-upload></div></div></div><div class=\"preview-img-panel\"><div class=\"preview-img-block\"><img id=\"previewLoadedImg\" class=\"img-responsive center-block\"></div><div class=\"pull-right\" ng-show=\"state == 'preview'\"><button type=\"button\" class=\"btn btn-default btn-sm\" ng-click=\"cancel()\">Cancel</button> <button type=\"button\" class=\"btn btn-default btn-sm\" ng-click=\"save()\">Save</button></div></div></div></div>"
  );


  $templateCache.put('application/templates/upload_local_file.tpl.html',
    "<div><input id=\"selectLocalFile\" type=\"file\" accept=\"image/png, image/gif, image/jpeg\"><div><a class=\"btn btn-link\" ng-click=\"showFileExplorer($event)\">Upload From PC</a></div></div>"
  );

}]);
