'use strict';

angular.module('File')
    .factory('BookmarkFactory', [function () {

        // Bookmark Class
        function Bookmark(file) {
            this._file = file;
            this.name = file.name;
            this.type = file.type;
            this.mimeType = file.mimeType;
            this.formatSize = file.formatSize;
            this.action = angular.copy(file.action);

            this.action.delete = true;
            delete this.action.bookmark;
        }

        Bookmark.prototype.getBlobURL = function() {
            return this._file.getBlobURL();
        };

        Bookmark.prototype.toBlob = function() {
            return this._file.toBlob();
        };

        return {
            create: function (file) {
                return new Bookmark(file);
            }
        }
    }]);