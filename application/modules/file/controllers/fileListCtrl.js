'use strict';

angular.module('File')
    .controller('FileListController', ['$scope', 'FileStorage', 'BookmarkStorage',
        function ($scope, FileStorage, BookmarkStorage) {
            $scope.files = FileStorage.getFiles();

            $scope.$on('addBookmark', function (event, data) {
                BookmarkStorage.add(data.file);
                $scope.systemNotification('File ' + data.file.name + ' was added to Bookmarks');
            });
        }]);