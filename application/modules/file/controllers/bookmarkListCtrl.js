'use strict';

angular.module('File')
    .controller('BookmarkListController', ['$scope', 'BookmarkStorage', function ($scope, BookmarkStorage) {
        $scope.bookmarks = BookmarkStorage.getFiles();

        $scope.$on('removeBookmark', function (event, data) {
            $scope.$applyAsync(function () {
                BookmarkStorage.remove(data.file);
                $scope.systemNotification('File ' + data.file.name + ' was removed from Bookmarks');
            });

        });
    }]);