'use strict';

angular.module('File')
    .controller('CreateFilePopupCtrl', ['FileFactory', 'FileStorage', '$scope',
        function (FileFactory, FileStorage, $scope) {
            $scope.types = [{
                type: 'Web page',
                mime: 'text/html',
                ext: '.html'
            }, {
                type: 'Text file',
                mime: 'text/plain',
                ext: '.txt'
            }
            ];

            $scope.fileName = '';
            $scope.fileType = $scope.types[0];

            $scope.saveFile = function (event) {
                var file;

                event.preventDefault();
                file = FileStorage.getFileByName($scope.fileName + $scope.fileType.ext);

                if (file) {
                    $scope.systemNotification('File ' + file.name + ' already exists', 'alert-warning');
                    return;
                }

                file = FileFactory.create({
                    name: $scope.fileName + $scope.fileType.ext,
                    mimeType: $scope.fileType.mime,
                    content: '',
                    size: 0
                });

                FileStorage.add(file);
                $scope.systemNotification('File ' + file.name + ' was created successfully');
                $scope.hideModal();
            }
        }]);