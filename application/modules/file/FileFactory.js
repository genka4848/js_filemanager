'use strict';

angular.module('File')
    .factory('FileFactory', [function () {
        var userTypes = {
            'text/plain': 'Text File',
            'text/html': 'Web page',
            'image/png': 'PNG image',
            'image/jpeg': 'JPEG image',
            'image/gif': 'GIF image'
        };

        // File Class
        function File(config) {
            this.name = config.name;
            this.mimeType = config.mimeType;
            this.type = userTypes[config.mimeType];
            this.content = config.content || '';
            this._size = config.size  || 0;
            this.formatSize = Math.ceil(this._size / 1024);
        }

        File.prototype.toBlob = function () {
            return new Blob([this.content], {type: this.mimeType});
        };

        File.prototype.getBlobURL = function () {
            var blob = this.toBlob(),
                url = URL.createObjectURL(blob);

            return url;
        };

        File.prototype.update = function() {
            this._size = this.toBlob().size;
            this.formatSize = Math.ceil(this._size / 1024);
        };

        File.prototype.export = function () {
            return {
                name: this.name,
                mimeType: this.mimeType,
                size: this._size,
                content: this.content
            };
        };

        // HTMLFile Class
        function HTMLFile(config) {
            this.action = {
                edit: true,
                preview: true,
                download: true,
                bookmark: true
            };

            File.apply(this, arguments);
        }

        HTMLFile.prototype = Object.create(File.prototype);
        HTMLFile.prototype.constructor = HTMLFile;

        // TextFile Class
        function TextFile(config) {
            this.action = {
                edit: true,
                download: true,
                bookmark: true
            };

            File.apply(this, arguments);
        }

        TextFile.prototype = Object.create(File.prototype);
        TextFile.prototype.constructor = TextFile;

        // ImageFile Class
        function ImageFile(config) {
            this.action = {
                preview: true,
                download: true,
                bookmark: true
            };

            File.apply(this, arguments);
        }

        ImageFile.prototype = Object.create(File.prototype);
        ImageFile.prototype.constructor = ImageFile;

        ImageFile.prototype.toBlob = function () {
            return this.content;
        };

        ImageFile.prototype.getBlobURL = function () {
            return this.content;
        };

        ImageFile.prototype.update = angular.noop;

        return {
            create: function (config) {
                var file = null;

                if (!config || !config.name || !config.mimeType) {
                    return file;
                }

                switch (config.mimeType) {
                    case 'text/plain':
                        file = new TextFile(config);
                        break;
                    case 'text/html':
                        file = new HTMLFile(config);
                        break;
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/gif':
                        file = new ImageFile(config);
                        break;
                }

                return file;
            }
        }
    }]);