'use strict';

angular.module('File')
    .factory('BookmarkStorage', ['LocalStorage', 'BookmarkFactory', 'FileStorage',
        function (LocalStorage, BookmarkFactory, FileStorage) {
        var bookmarksKey = 'bookmarks',
            tempBookmarks = [];

        function saveToStorage() {
            var bookmarks = [];

            tempBookmarks.forEach(function (file) {
                bookmarks.push(file.name);
            });

            LocalStorage.set(bookmarksKey, bookmarks);
        };

        return {
            add: function (file) {
                tempBookmarks.push(BookmarkFactory.create(file));

                saveToStorage();
            },
            remove: function (file) {
                var index = tempBookmarks.indexOf(file);

                if (index !== -1) {
                    tempBookmarks.splice(index, 1);
                    saveToStorage();
                }
            },
            getFiles: function () {
                return tempBookmarks;
            },
            synchronize: function(file) {
                var i, len = tempBookmarks.length;

                for(i = 0; i < len; i++) {
                    if (file.name === tempBookmarks[i].name) {
                        tempBookmarks[i].formatSize = file.formatSize;
                        break;
                    }
                }
            },
            loadFromStorage: function () {
                var files, i, len,
                    bookmarks = LocalStorage.get(bookmarksKey);

                if (!bookmarks) {
                    return null;
                }

                files = FileStorage.getFiles();
                len = files.length;

                tempBookmarks = [];
                bookmarks.forEach(function (fileName) {
                    for(i = 0; i < len; i++) {
                        if (fileName == files[i].name) {
                            tempBookmarks.push(BookmarkFactory.create(files[i]));
                            break;
                        }
                    }
                });

                return tempBookmarks;
            }
        }
    }]);