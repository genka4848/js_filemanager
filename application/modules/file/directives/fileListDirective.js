'use strict';

angular.module('File')
    .directive('fileList', ['$sce', '$location', '$rootScope', function($sce, $location, $rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'application/templates/file_list_dir.tpl.html',
            scope: {
                files: '=data'
            },
            link: function($scope, $element, $attr) {
                var targetSelected = null, pattern = '',
                    downloadLink = angular.element('.download-link', $element);

                $scope.predicate = '';
                $scope.reverse = false;
                $scope.regex = '';
                $scope.filterByName = '';
                $scope.fileSelected = null;

                function clearSelect() {
                    if (targetSelected) {
                        targetSelected.removeClass('active');
                        $scope.fileSelected = null;
                        downloadLink.removeAttr('download');
                        downloadLink.removeAttr('href');
                    }
                };

                function filterByName(element) {
                    return pattern.test(element.name) ? true : false;
                };

                function docOnClick(e) {
                    if (e.target.tagName == 'TBODY' || $(e.target).hasClass('btn-action')) {
                        return;
                    }

                    if (e.target.tagName != 'TD') {
                        if (!$scope.$$phase && !$scope.$root.$$phase) {
                            $scope.$apply(clearSelect);
                        } else {
                            clearSelect();
                        }
                    }
                };

                $(document).on('click' , docOnClick);

                $scope.select = function(event, file) {
                    clearSelect();

                    $scope.fileSelected = file;
                    targetSelected = $(event.target).parent();
                    targetSelected.addClass('active');

                    downloadLink.attr('download', $scope.fileSelected.name);
                    downloadLink.attr('href', $scope.fileSelected.getBlobURL());
                };

                $scope.addBookmark = function() {
                    if ($scope.fileSelected) {
                        $scope.$emit('addBookmark', { file: $scope.fileSelected });
                    }
                };

                $scope.removeBookmark = function() {
                    if ($scope.fileSelected) {
                        $scope.$emit('removeBookmark', { file: $scope.fileSelected });
                    }
                };

                $scope.preview = function() {
                    var tpl;

                    if ($scope.fileSelected) {
                        tpl = $scope.fileSelected.mimeType === 'text/html' ? 'application/templates/preview_web_page.tpl.html': 'application/templates/preview_img.tpl.html';
                        $rootScope.showModal({
                            template: tpl,
                            title: $scope.fileSelected.name,
                            src: $sce.trustAsResourceUrl($scope.fileSelected.getBlobURL())
                        });
                    }
                };

                $scope.edit = function() {
                    if ($scope.fileSelected) {
                        $location.path('/edit/' + $scope.fileSelected.name);
                    }
                };

                $scope.order = function(field) {
                    $scope.reverse = ($scope.predicate === field) ? !$scope.reverse : false;
                    $scope.predicate = field;
                };

                $scope.$watch('regex', function(newValue, oldValue) {
                    if (newValue === oldValue) {
                        return;
                    }

                    if (newValue) {
                        pattern = newValue.replace(/\.|\+|\?/g, '\\$&');
                        pattern = pattern.replace(/\*/g, '.*');
                        pattern = '^' + pattern + '$';
                        pattern = new RegExp(pattern, 'i');
                        $scope.filterByName = filterByName;

                    } else {
                        $scope.filterByName = '';
                    }
                });

                $scope.$on('$destroy', function() {
                    $(document).off('click' , docOnClick);
                });
            }
        }
    }]);