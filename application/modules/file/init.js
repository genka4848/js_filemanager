'use strict';

angular.module('File', ['Storage'])
    .config(['LocalStorageProvider', function (LocalStorageProvider) {
        LocalStorageProvider.setNamespace('fm_');
    }])
    .run(['FileStorage', 'BookmarkStorage', function(FileStorage, BookmarkStorage) {
        FileStorage.loadFromStorage();
        BookmarkStorage.loadFromStorage();
    }]);