'use strict';

angular.module('File')
    .factory('FileStorage', ['LocalStorage', 'FileFactory', function (LocalStorage, FileFactory) {
        var filesKey = 'files',
            tempFiles = [];

        function saveToStorage() {
            var files = [];

            tempFiles.forEach(function (file) {
                files.push(file.export());
            });

            LocalStorage.set(filesKey, files);
        };

        return {
            add: function (file) {
                tempFiles.push(file);

                saveToStorage();
            },
            getFiles: function (filter) {
                if (filter && angular.isFunction(filter)) {
                    return tempFiles.filter(filter);
                } else {
                    return tempFiles;
                }
            },
            update: function(file) {
                if (file && angular.isFunction(file.update)) {
                    file.update();
                    saveToStorage();
                }
            },
            getFileByName: function(name) {
                var i,
                    file = null,
                    len = tempFiles.length;

                for(i = 0; i < len; i++) {
                    if (name === tempFiles[i].name) {
                        file = tempFiles[i];
                        break;
                    }
                }

                return file;
            },
            loadFromStorage: function () {
                var files = LocalStorage.get(filesKey);

                if (!files) {
                    return null;
                }

                tempFiles = [];
                files.forEach(function (file) {
                    tempFiles.push(FileFactory.create(file));
                });

                return tempFiles;
            }
        }
    }]);