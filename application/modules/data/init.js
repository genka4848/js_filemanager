angular.module('TestData', [])
    .run(['LocalStorage', function(LocalStorage) {
        var files, bookmarks;
        if (!LocalStorage.get('files')) {
            files = [{
                    "name":"about.html",
                    "mimeType":"text/html",
                    "size":33,
                    "content":"<b>Hello From Web Page!!!</b>"
                },
                {
                    "name":"simple.txt",
                    "mimeType":"text/plain",
                    "size":16,
                    "content":"Simple text File"
                },
                {
                    "name":"twitter.png",
                    "mimeType":"image/png",
                    "size": 1628,
                    "content":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAMAAAAp4XiDAAAAolBMVEX///8ArO7+//////3//v8ArOwAq/AAre8ArPAAru0ApOz///sAoOwBqu0Aq+sAqe4AnOsAou0BmewApu0Anecuu/H3/P/u+f1FwfTD7/qD1/lxzvmk4PqR3Pnc8v8Ao/DG6/uz5vknvfBcyPZoy/IAlOkZtO181fff9vzL7/1YzPVz0fey5PuT2/Q5t/KG1PiR1vep5vjK6/xPvfKn3fl90fZ5g08dAAAFdUlEQVRIiW1Vi3baOBSUrLcsWbIN2CWGQiCEkDQN7fb/f23nyiHJ2a7gcADrvmfmMiGqqpJMiopJWTH6sn1ZHw/jOB7X01YwxkQlmGDzs/JbVkKyGl8rCVuxfVzGEDJe7SKH5vX5F57AQIq6EuQcP2EtxHsEOS373EaujeXaGm57n9vl20pKioI8GEUpoWBUS8n2LgfOudZORaO1Ujxyo0M2U7lGXotJcSCkEMNhEZRLJnLucJMbzo1RVnHX5sMZaQtRLOZYKKv6prvkuObKRGOsdo6XgNzhbbt+T46pBzg1skLYqw9w6aJx3HI1R4mOx6SVRtg+b2RVC1miCHqxTfbGWRUtp0Dc0j3YGquc4QrR++4R6dRz5RiKvHbe8UY7dOp2osZFo9AFS35M322ELIkhP9QRPDpqTbSfJkgwcqt5Q71DKJPydW4vTWRlW9Mohb9VScjMJtH2feh1QIWaWu7boVhgouKSLfJ+b2sJYZBl45I/Ha73v10De3qcx5qV6ctdTshH4cnsv9gYdKmdVpU870wqndAm5qmaB3NqnYIJzyHN0yh5xZR/IOvds+/6MpvGGs9XJbVrZzFi5cfNMuVWa4oEmPHuiEnI89PumMhno7Xl8ELF3Ae6ZNqlZOc/buH1HKV1kiqVtWRPJiMOAKHbE+V17mnYFvAjTAxvBZooK18L8sAj1NMHmjMAHnb4/Zgxv0Zp590gaUj/jDk4t1giK7CP5ibYrk0YK4LnA8IsW2cNVW0ysFcANLnch72UM9Zr4u1zJgOjelexcxOVAbwsT+7PC3pY1axeHR9McU/0RttYde4TQIMW+G/sGjTVqxXvqd75iqjvRqRTY2oCGCSiuIcWWLcub9g6Y34GcFImP5a06plATBbMogNEeDn9vr+sTXL5yI6BCgHGtUr+7hdhtbiWYuaeJNKDKbDe3vWah5FdAmEL8yP+Pbh9YZEEbYtMFUDJmgz3B2hCo/2SjQte+oVRaT8Oq8LWkst8ilJR74QcJpNsC5PQIEwE7J32r2fSgpv4vB8YINKwux418IkoB1AeCFMAKrchTsNQ9Kb6tCA9FezysAjJADJLdskNYVDP0/RtelyVaj4s4AAsqZwnOkeH8h9BL6oGdkjN+Vh9KYT4VHgrrxnyBt7qfGFT4DRKMFhRr3szkDZ/KQUMgAifWoibUc7lNTunGMHYQl5kF5MZX3Dnay1o2aVDwaAl134HWPreXk45wgcgn8LD6xOh6jMztG/qdMne8d5gDM+Zf19++9nnEMICOj9Rl8saEUUZEXDvPdRT2QZov+C/p5ar8MrkBlvosCYGVdTiWYFL867hu8PkSD513hOK7heNyeZcCfEua5ImCYwRaGD3lr1Dc2xjiC4l16mDjPpwV7aBLCTEtiFM0sf2NffOAek0NwuRJeGvoAzGpbwkgtFWK0uQFdgPf2KnnYYC4dO4Pg2l7WKfoVJO5/C62dI+EKVLjK1eji4nrBDIdZFS121mHa/Y2GmNFWfyQo/P0247DOenl/XFhNyisYrevMEmWJyIo2UvDT1Sgz6C/7jlDQbdt4vgaV1Y2jW02wiA5xu0GVLrDS0vS0XGHid9CC0RCUlpF7sfMyeqMrgfXSqo44r/5xhaIijE8e6ZvZNinsa6Swgd9W0lfZhSGSZpl7pjxd6hR+RG29bBk743N+c3E41aYnKeYlTyY72Swokp5VkFyn3zGQXi5UKLhczEO5Uq0qEaptsRW9l+Xp43k0OqviN0C9rHN1JA6OYmRNov9otRjKppu/izKpJGYjM3+bbLWTW8nULwSX8EiTHn1/VZzG0qsPj77A8nn/PCe0hwRqaHF1Yg+j9XP0goVrvNcVzijIe33fD3jX8BcTxPJMv20a4AAAAASUVORK5CYII="
                },
                {
                    "name":"duck.jpg",
                    "mimeType":"image/jpeg",
                    "size":2026,
                    "content":"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gAyUHJvY2Vzc2VkIEJ5IGVCYXkgd2l0aCBJbWFnZU1hZ2ljaywgejEuMS4wLiB8fEIx/9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8AAEQgAYABgAwEiAAIRAQMRAf/EABwAAQACAwEBAQAAAAAAAAAAAAAGBwQFCAMBAv/EADUQAAEDAwMCBAQFAgcAAAAAAAEAAgMEBREGEiExQQcTUWEiMnGRFBVSgbEjoQgWJDRCYoL/xAAaAQACAwEBAAAAAAAAAAAAAAAABQECBAMG/8QALBEAAQMDBAAFAgcAAAAAAAAAAQACAwQRIQUSMVEGE0GBoSLRFCMyYXHB4f/aAAwDAQACEQMRAD8A6pREQhEREIVceL/iQzQ9PTwUsLJ7lUNL2iTOyNnTcQOpJ6D2P70a/wAZtXSzmSO6bA452CGPAHoBtW5/xMUVyrvEO2UtLTvnkq6WKGkjZyXuL35A98n7KV+EHhZNpiSrqNTQUNVVyANhdE/zWsbj4gcgYdn0BGB1T2J1NS07XPaHOPuk8jJ6iVwa4gA/wvDQPjZUT1sVJqdsToZCG/iY2bHRn1cBwR9AMe6vtrgQCDkHoQudNd+Gl5u2pa+ssFsoKWgja1jGOnax9Q7GXOaBwOuOSOiv2wQzU9jt0NU4PqI6aNkjh3cGgE/dY678O4NkgwTyOl3ovPY50c2QOCs9ERLkxRERCEREQhEREIUa1LY6ervlnvL/APcW5szYwQCD5jQ3P1Hb6laCG+zC5eS5oETiWtIPP1P2UzvY/wBLu7DqqN1OzUv+etMiw0z57X+Kd+ZvbHuDWDbjJxxxuIx3wqvla0fWeBhQ2Mk/QOeVZVRdHsqYo2AFzueT0Cl9tf5lK0ql3v1M/wAVYInUsjNMC27vOMPwmff034+b29FcloGKXnscf2UMe14BaVYtc0kFZyIiuoRERCEREQhERQvxR1HV6ZtFJWUrMwGpY2rkAy6OLvtHqemff3VmMc87W8qkkjY2l7uAsPxS1szTVCBT/llW4ZFXTTVQZIxhwAQ3OTyefblQCj1/abqxzBdIKGojyHU9QfK2kfpOQCPoqQ1Y4vuL4Z53SipLtlSTxMHZ+Mk9z3z3Wgu0LZp98TtuMOG7/kCO5WbXtNgkhic11wb5HsU18MA1ZnEgIcLED0tkH+l0hWeIFps9P5jrxBWVBIDIKSQSudn1wSGj3KsvR+vNNX2omt9quTXzwODCJmmIyk/oDsE8+i4WtrIwWvYdzm9geApBZ6jbqGd76n8PTMk3yStw5zRx8o7u9AuOh0NPCXscSLDn/PW60eIIHQU0c8di4mxF8EW+LLvsIq58I9V3PUVufLW0RgtjGhlJPK8mWUDj4v1dOXcc+qsZMJI3Ru2u5SGKQSt3BERFRdEREQhD0US1VFFcaCqgq42y08rCxzHdCFLJASxwHUhRStjkml/DMY4yHjp0UEluQi27BXLutNHwWJlHK2eaZtRJUmGOU5EbGeWAR/6Luf8AqoUbewRF28OJJJXW+vNL0N0o6K1VrHGOGNxZLGdr2uccuIP17Kpbl4Nyec40N3Bj7Nmh5H7g8/ZPKKvpxHtlw6/X2SapoqjzC6L9JsqhoIAyRxkcw8HGBz+6sXwa0bBq253C11lM423Z+MM0eGmKYEBoz6OBeCO/XqMrfWnwcjbIHV91c5uflgi25/ck/wAK4/D+y0Vg2UlrgEcJ5eernnHzOPc9EVeowFmyLJ7RT0E3mbpcN67Wx09EKagggDGx+W0R7BwBjjCklHOJG7e4Wtq7dNFJJLSkPDnbtnQj6LLtscpdvlYWYGMFIrm+U6stgiIrKEREQhEwOvdF8e4NaS4gAdyhCrfxDuEr7pHHTymM0zeo7k9QfZe1gobjc7cyqdHGNxLRh3XHdaTVsM015qpmRSPjdISHNGQQrH0nGI9O29o4/pAn6nk/yvKaXPNU18weTtF8e9h8J1WRshpY9oyVGbtablSW2oqY2xAxMLsE5OO/A+61OirtLT3VvnzF4mIa4uPr/CtGRrXtLXgFrhgg9wqWNJPT3WaGGNz/ACpS0OaODg8co16SaklimiJI6/cfcI01rKhj43jPausIsO2VjKqmjId/V2je09Qe6zF6iN7ZGhzeCkzmlpsUREV1CIiIQvzI8RxueejQSVBqq+vrnPcSWxA4a1TsjIUPvemJ5nyOoHxta4l2w/DglKdWhqZYx5B45Ha20T4mO/MHutI6vhlfsGNyl+kt35c8E/D5hwPTgKI0+ibrLVNfUVMEMY67XF5P9grCoKVlFSxwR5LWDqep90v0akqo5TJUNsFqr54XMDIjdfa5xZRzub8wjcR9lWbapkZxt5yrRljEsbmO+VwIP0VY3bSV4hqM0jWVUAPG1wa7HuCr6/BUybHQN3Wvf4VdMliZuEhtdZdPdHQOEsR5bzwp1aq1lwoo6iLO138qC2XTFydJuq4/LYRgtc4H+FPqKmjpKaOCFoaxgwAFbQ4qtt3Tja0+h5v2o1F8DrCPJ7XuiIvQpWv/2Q=="
                }
            ];

            bookmarks = ["simple.txt","about.html", "twitter.png"];

            LocalStorage.set('files', files);
            LocalStorage.set('bookmarks', bookmarks)
        };

    }]);
