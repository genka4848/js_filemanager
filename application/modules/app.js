'use strict';

angular.module('app', [
    'ngRoute', 'TestData', 'File', 'Upload', 'Messenger', 'SimpleModal', 'Edit'
])
    .config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'application/templates/file_list.tpl.html',
                    controller: 'FileListController'
                })
                .when('/bookmarks', {
                    templateUrl: 'application/templates/bookmarks.tpl.html',
                    controller: 'BookmarkListController'
                })
                .when('/upload', {
                    templateUrl: 'application/templates/upload.tpl.html',
                    controller: 'UploadController'
                })
                .when('/edit/:fileName', {
                    templateUrl: 'application/templates/edit_file.tpl.html',
                    controller: 'EditFileCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });

            $locationProvider.html5Mode(false);
            $locationProvider.hashPrefix('!');
        }])
    .controller('MainController', ['FileStorage', 'FileFactory', '$scope',
        function (FileStorage, FileFactory, $scope) {

            $scope.createFile = function () {
                $scope.showModal({
                    title: 'Create File',
                    template: 'application/templates/create_file.tpl.html'
                });
            };
        }]);