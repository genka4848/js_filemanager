'use strict';

angular.module('Upload')
    .directive('localFileUpload', ['UploadService', function(UploadService) {
        return {
            restrict: 'E',
            templateUrl: 'application/templates/upload_local_file.tpl.html',
            $scope: true,
            link: function ($scope, $element, $attr) {
                var inputFile = $('#selectLocalFile', $element);

                $scope.showFileExplorer = function(e) {
                    e.preventDefault();
                    inputFile.trigger('click');
                };

                inputFile.on('change', function(event) {
                    var file = event.target.files[0];

                    UploadService.loadFile(file)
                        .then(function(data) {
                            var config = {
                                name: file.name,
                                mimeType: file.type,
                                size: file.size,
                                content: data
                            };

                            $scope.$emit('fileLoaded', config);
                        },
                        function(err) {
                            $scope.$emit('fileLoadedError', err);
                        });
                });
            }
        }
    }]);