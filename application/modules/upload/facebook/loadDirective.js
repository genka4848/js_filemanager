angular.module('Upload')
    .directive('facebookFileLoad', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'application/templates/load_fb_file.tpl.html',
            $scope: true,
            link: function ($scope, $element, $attr) {

                $scope.showUserImages = function (e) {
                    e.preventDefault();

                    $rootScope.showModal({
                        title: 'Select image from Facebook',
                        template: 'application/templates/fb_user_images.tpl.html'
                    }).then(function(data) {
                        if (!data.res.err) {
                            $scope.$emit('fileLoaded', data.res);
                        } else {
                            $scope.$emit('fileLoadedError', data.res);
                        }
                    })
                };

            }
        }
    }]);