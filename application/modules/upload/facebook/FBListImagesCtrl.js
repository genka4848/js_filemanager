angular.module('Upload')
    .controller('FBListImagesCtrl', ['FBFileService', 'UploadService', '$scope',
        function (FBFileService, UploadService, $scope) {
            $scope.images = [];

            $scope.select = function (img) {
                var xhr = new XMLHttpRequest(),
                    blob;

                xhr.open("GET", img.picture, true);

                xhr.responseType = "arraybuffer";

                xhr.addEventListener("load", function () {
                    if (xhr.status === 200) {

                        blob = new Blob([xhr.response], {type: "image/png"});
                        UploadService.loadFile(blob)
                            .then(function(data) {
                                var config = {
                                    name: 'fb_image_' + Math.floor((Math.random() * 1000) + 1) +'.png',
                                    mimeType: blob.type,
                                    size: blob.size,
                                    content: data
                                };

                                $scope.setResult(config);
                            },
                            function(err) {
                                err.err = true;
                                scope.setResult(err);
                            });
                    }
                }, false);
                xhr.send();
            };

            FBFileService.loadList()
                .then(function (res) {
                    $scope.images = res.data;
                    console.log('FB List Load Success ', res);
                },
                function (err) {
                    console.log('FB List Load Error ', err);
                });
        }]);