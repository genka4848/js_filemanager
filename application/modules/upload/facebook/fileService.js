angular.module('Upload')
    .factory('FBFileService', ['$q', function($q) {
        var service = {}, accessToken = null;

        function authorize(defered) {
            window.FB.login(function(response) {
                    if (response.authResponse) {
                        accessToken = response.authResponse.accessToken;
                        getListImages(defered);
                    } else {
                        accessToken = null;
                        defered.reject('Authorization failed');
                    }
                }
            ,  {scope: 'user_photos'});

            return defered.promise;
        };

        function checkAuth() {
            var defered = $q.defer();

            window.FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    accessToken = response.authResponse.accessToken;
                    getListImages(defered);
                } else {
                    authorize(defered);
                }
            });

            return defered.promise;
        };

        function getListImages(def) {
            var deferred = def || $q.defer();

            window.FB.api('me/photos/uploaded?fields=name,picture&access_token=' + accessToken,
                function (response) {
                    console.log('Facebook resp ', response);
                    if (response.error) {
                        deferred.reject(response.error);
                    } else {
                        deferred.resolve(response);
                    }
                });
            return deferred.promise;
        }

        service.loadList = function () { console.log('Acc= ', accessToken);
            if (accessToken) {
                return getListImages();
            } else {
                return checkAuth();
            }
        };

        return service;
    }]);
