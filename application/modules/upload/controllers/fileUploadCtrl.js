'use strict';

angular.module('Upload')
    .controller('UploadController', ['FileStorage', 'FileFactory', '$location', '$scope',
        function (FileStorage, FileFactory, $location, $scope) {
            var imgFile = null;

            $scope.state = 'select';

            $scope.cancel = function () {
                imgFile = null;
                $('#previewLoadedImg').attr('src', '');
                $scope.state = 'select';
            };

            $scope.save = function () {
                var newFile;

                if (!imgFile) {
                    return;
                }

                newFile = FileFactory.create(imgFile);

                if (!newFile) {
                    return;
                }

                FileStorage.add(newFile);
                $scope.systemNotification('File ' + newFile.name + ' was saved successfully');
                $location.path("/");
            };

            $scope.$on('fileLoaded', function (event, data) {
                imgFile = data;

                $('#previewLoadedImg').attr('src', data.content);
                $scope.state = 'preview';
            });

            $scope.$on('fileLoadedError', function (event, err) {
                console.log('Fail Load Error', err);
            });

            $scope.systemNotification('For loading images from facebook you need re-login, for getting access token',
            'alert-warning', 5000)
        }]);