'use strict';

angular.module('Upload')
        .service('UploadService', ['$window', '$q', function ($window, $q) {
        this.loadFile = function (file) {
            var defered = $q.defer(),
                fileReader = new $window.FileReader();

            fileReader.onload = function (event) {
                var data = event.target.result || event.srcElement.result;
                defered.resolve(data);
            };

            fileReader.onerror = function () {
                var error = event.target.error || event.srcElement.error;
                defered.resolve(error);
            };

            fileReader.readAsDataURL(file);

            return defered.promise;
        };
    }]);