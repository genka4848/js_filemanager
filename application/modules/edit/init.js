'use strict';

angular.module('Edit', ['jerryhsia.minieditor'])
    .controller('EditFileCtrl', ['FileStorage', 'BookmarkStorage', '$routeParams', '$location', '$scope',
        function (FileStorage, BookmarkStorage, $routeParams, $location, $scope) {
            var file;

            $scope.options = {
                menus: [
                    ['bold', 'italic', 'underline', 'strikethrough', 'removeformat']
                ]
            };

            file = FileStorage.getFileByName($routeParams.fileName);

            if (!file) {
                $scope.content = '';
                $scope.systemNotification('File ' + $routeParams.fileName + ' not found', 'alert-warning');
            } else {
                $scope.content = file.content;
                if (file.mimeType === 'text/html') {
                    $scope.options.menus.push( ['insertimage']);
                }
            }

            $scope.save = function () {
                if (file) {
                    file.content = $scope.content;
                    FileStorage.update(file);
                    BookmarkStorage.synchronize(file);
                    $scope.systemNotification('File ' + file.name + ' was updated');
                }
            }
        }]);

