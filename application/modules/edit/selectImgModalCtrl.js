'use strict';

angular.module('Edit')
    .controller('SelectImgModalCtrl', ['FileStorage', '$scope', function(FileStorage, $scope) {
        $scope.images = FileStorage.getFiles(function(file) {
            return (file.mimeType == 'image/png' || file.mimeType == 'image/jpeg' || file.mimeType == 'image/gif');
        });

        $scope.setImage = function(img) {
            $scope.setResult(img.getBlobURL());
        }
    }]);