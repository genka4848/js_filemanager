'use strict';

angular.module('Storage', [])
    .provider('LocalStorage', [function() {
        var prefix = '';

        this.setNamespace = function(namespace) {
            if (namespace && angular.isString(namespace)) {
                prefix = namespace;
            }
        };

        this.$get = ['$window', function($window) {
            return {
                set: function(key, value) {
                    if (!key || !angular.isString(key) || angular.isUndefined(value)) {
                        return;
                    }

                    if (angular.isObject(value)) {
                        value = angular.toJson(value);
                    }

                    key = prefix + key;
                    $window.localStorage.setItem(key, value);
                },
                get: function(key) {
                    if(!key || !angular.isString(key)) {
                        return null;
                    }

                    key = prefix + key;
                    return angular.fromJson($window.localStorage.getItem(key));
                }
            }
        }];
    }]);