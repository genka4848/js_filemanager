'use strict';

angular.module('SimpleModal', [])
    .run(['$rootScope', '$q', function($rootScope, $q) {
        var defered;

        $rootScope.showModal = function(settings) {
            defered = $q.defer();
            $rootScope.$emit('showModal', settings);

            return defered.promise;
        };

        $rootScope.hideModal = function() {
            $rootScope.$emit('hideModal');
        };

        $rootScope.$on('resolveModal', function (event, data){
            defered.resolve(data)
        });
    }])
    .directive('previewWrapper', ['$rootScope', function ($rootScope) {
        return {
            templateUrl: 'application/templates/modal_wrapper.tpl.html',
            restrict: 'E',
            replace: true,
            link: function postLink($scope, $element, $attrs) {
                var modal = $($element);

                $scope.setResult = function(res) {
                    $rootScope.$broadcast('resolveModal', {res : res});
                    modal.modal('hide');
                };

                $scope.$on('showModal', function(event, data) {
                    $scope.template = data.template;
                    $scope.title = data.title || '';
                    $scope.src = data.src || '';
                    modal.modal('show');
                });

                $scope.$on('hideModal', function(event, data) {
                    modal.modal('hide');
                });


                $($element).on('hidden.bs.modal', function (event) {
                    $scope.template = '';
                    $scope.title = '';
                    $scope.src = '';
                });
            }
        };
    }]);